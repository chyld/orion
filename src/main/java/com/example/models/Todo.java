package com.example.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "todos")
public class Todo {
    private int id;
    private String name;
    private String category;
    private Date due;
    private boolean isComplete;
    private Date createdAt;
    private Date updatedAt;

    public Todo() {
    }

    @Id
    @GeneratedValue
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}

    public String getName() {return name;}
    public void setName(String name) {this.name = name;}

    public String getCategory() {return category;}
    public void setCategory(String category) {this.category = category;}

    @Temporal(TemporalType.DATE)
    public Date getDue() {return due;}
    public void setDue(Date due) {this.due = due;}

    @Column(name = "is_complete", columnDefinition = "BIT", length = 1)
    public boolean getIsComplete() {return isComplete;}
    public void setIsComplete(boolean complete) {isComplete = complete;}

    @Column(name = "created_at", insertable = true, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreatedAt() {return createdAt;}
    public void setCreatedAt(Date createdAt) {this.createdAt = createdAt;}

    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getUpdatedAt() {return updatedAt;}
    public void setUpdatedAt(Date updatedAt) {this.updatedAt = updatedAt;}

    @PrePersist
    protected void onCreate(){
        createdAt = new Date();
        updatedAt = new Date();
    }

    @PreUpdate
    protected void onUpdate(){
        updatedAt = new Date();
    }
}
