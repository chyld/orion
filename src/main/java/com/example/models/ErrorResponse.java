package com.example.models;

public class ErrorResponse {
    private String message;

    public ErrorResponse(String msg) {
        this.message = msg;
    }

    public String getMessage() {
        return message;
    }
}
