package com.example.controllers;

import com.example.models.Todo;
import com.example.repositories.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping({"/todos"})
public class TodoController {
    private TodoRepository repo;

    @Autowired
    public void setRepo(TodoRepository repo) {
        this.repo = repo;
    }

    @RequestMapping(path = {"", "/"}, method = RequestMethod.GET)
    public Iterable<com.example.models.Todo> index(@RequestParam(name = "category", required = false) String category,
                                                   @RequestParam(name = "page", required = false, defaultValue = "0") int page){
        PageRequest pr = new PageRequest(page, 2, Sort.Direction.DESC, "id");

        if(category != null) {
            return repo.findByCategory(category, pr);
        } else {
            return repo.findAll(pr);
        }
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Todo show(@PathVariable int id) throws Exception{
        Todo t = repo.findOne(id);
        if(t == null)
            throw new Exception("todo not found: " + id);
        return t;
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id){
        repo.delete(id);
    }

    @RequestMapping(path = {"", "/"}, method = RequestMethod.POST)
    public Todo create(@RequestBody Todo todo){
        return repo.save(todo);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public Todo update(@PathVariable int id, @RequestBody Todo t){
        Todo todo = repo.findOne(id);
        todo.setName(t.getName());
        todo.setCategory(t.getCategory());
        todo.setDue(t.getDue());
        return repo.save(todo);
    }

    @RequestMapping(path = "/{id}/complete", method = RequestMethod.PATCH)
    public Todo complete(@PathVariable int id){
        Todo todo = repo.findOne(id);
        todo.setIsComplete(!todo.getIsComplete());
        return repo.save(todo);
    }
}
