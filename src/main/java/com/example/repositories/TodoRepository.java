package com.example.repositories;

import com.example.models.Todo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TodoRepository extends PagingAndSortingRepository<Todo, Integer> {
    public Page<Todo> findByCategory(String category, Pageable pageable);
}
