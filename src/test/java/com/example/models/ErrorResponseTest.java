package com.example.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ErrorResponseTest {
    @Test
    public void shouldCreateErrorResponseInstance() throws Exception {
        ErrorResponse er = new ErrorResponse("an error has occurred");
        assertEquals("an error has occurred", er.getMessage());
    }
}
