package com.example.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Date;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class TodoTest {
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void shouldCreateTodoInstance() throws Exception {
        Todo t = new Todo();
        assertThat(t, instanceOf(Todo.class));
    }

    @Test
    public void shouldGetAndSetId() throws Exception {
        Todo t = new Todo();
        t.setId(3);
        assertEquals(3, t.getId());
    }

    @Test
    public void shouldCreateDate() throws Exception {
        Todo t = new Todo();
        t.onCreate();
        assertTrue(t.getCreatedAt().getTime() > 0);
    }

    @Test
    public void shouldUpdateDate() throws Exception {
        Todo t = new Todo();
        t.onUpdate();
        assertTrue(t.getUpdatedAt().getTime() > 0);
    }
}
