package com.example.controllers;

import com.example.models.Todo;
import com.example.repositories.TodoRepository;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
@Sql(value = {"/truncate-todos.sql"})
public class TodoControllerTest {
    @Autowired
    private TodoRepository repo;

    @Before
    public void setUp() throws Exception {
        RestAssured.port = 8001;
        Todo t = new Todo();
        t.setName("get milk");
        t.setCategory("groceries");
        t.setDue(new Date());
        repo.save(t);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    // GET /api/todos
    public void shouldGetAllTheTodos() throws Exception {
        get("/api/todos")
                .then()
                .statusCode(200)
                .body("numberOfElements", is(1));
    }

    @Test
    // GET /api/todos/1
    public void shouldGetASingleTodo() throws Exception {
        get("/api/todos/1")
                .then()
                .statusCode(200)
                .body("id", is(1));
    }

    @Test
    // POST /api/todos
    public void shouldCreateATodo() throws Exception {
        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("name", "water");
        jsonAsMap.put("due", "2012-01-03");
        jsonAsMap.put("category", "drinks");

        given().
            contentType(ContentType.JSON).
            body(jsonAsMap).
        when().
            post("/api/todos").
        then().
            statusCode(200).
            body("id", is(2));
    }

    @Test
    // DELETE /api/todos/1
    public void shouldDeleteATodo() throws Exception {
        when().
            delete("/api/todos/1").
        then().
            statusCode(200);
    }

    @Test
    // DELETE /api/todos/7
    public void shouldNotDeleteATodo() throws Exception {
        when().
                delete("/api/todos/7").
                then().
                statusCode(400);
    }

    @Test
    // PATCH /api/todos/1/complete
    public void shouldChangeTodoStatus() throws Exception {
        when().
            patch("/api/todos/1/complete").
        then().
            statusCode(200).
            body("isComplete", is(true));
    }
}

//        {
//            "id": 1,
//            "name": "get milk",
//            "category": "groceries",
//            "due": "2016-08-15",
//            "isComplete": false,
//            "createdAt": 1471315824000,
//            "updatedAt": 1471315824000
//        }
